<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRoomsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('rooms', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('number')->unique();
            $table->integer('floor_number');
            $table->string('type', 45);
            $table->integer('number_beds');
            $table->boolean('air_conditioner');
            $table->enum('status', \models\Room::STATUS)->default(\models\Room::STATUS_FREE);
            $table->string('comments', 200)->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('rooms');
    }
}
