<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name', 50);
            $table->string('lastname', 45);
            $table->string('identification_type', 20);
            $table->string('identification_number', 20)->unique();
            $table->integer('phone')->nullable();
            $table->integer('cellphone')->nullable();
            $table->string('address', 100);
            $table->timestamp('birth_date');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
