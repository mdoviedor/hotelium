<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        factory(\models\User::class)->create([
            'name' => 'Marlon Oviedo',
            'email' => 'marlon.oviedo5@gmail.com',
        ]);
    }
}
