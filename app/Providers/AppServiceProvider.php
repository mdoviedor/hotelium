<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Monolog\Logger;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     */
    public function boot()
    {
        $this->loadViewsFrom(base_path('src/www/auth/templates'), 'auth');
        $this->loadViewsFrom(base_path('src/www/home/templates'), 'home');
        $this->loadViewsFrom(base_path('src/www/rooms/templates'), 'rooms');
        $this->loadViewsFrom(base_path('src/www/clients/templates'), 'clients');
        $this->loadViewsFrom(base_path('src/www/__templates'), 'templates');

        $this->app->when(Logger::class)
            ->needs('$name')
            ->give('logs');
    }

    /**
     * Register any application services.
     */
    public function register()
    {
    }
}
