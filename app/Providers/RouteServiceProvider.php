<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
//    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     */
    public function boot()
    {
        $this->homeRoutes();
        $this->authRoutes();
        $this->roomsRoutes();
        $this->clientsRoutes();

        parent::boot();
    }

    /**
     * Define the routes for the application.
     */
    public function map()
    {
    }

    protected function homeRoutes()
    {
        Route::middleware(['web'])
            ->group(base_path('src/www/home/routes.php'));
    }

    protected function authRoutes()
    {
        Route::middleware(['web'])
            ->group(base_path('src/www/auth/routes.php'));
    }

    protected function roomsRoutes()
    {
        Route::prefix('rooms')
            ->middleware(['web', 'auth'])
            ->group(base_path('src/www/rooms/routes.php'));
    }

    protected function clientsRoutes()
    {
        Route::prefix('clients')
            ->middleware(['web', 'auth'])
            ->group(base_path('src/www/clients/routes.php'));
    }
}
