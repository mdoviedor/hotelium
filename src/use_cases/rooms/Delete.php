<?php
/**
 * Created by PhpStorm.
 * User: marlon
 * Date: 9/02/18
 * Time: 08:47 AM.
 */

namespace use_cases\rooms;

use models\Room;
use repositories\ReservationRepository;
use repositories\RoomRepository;

/**
 * Class Delete.
 */
class Delete
{
    /**
     * @var RoomRepository
     */
    private $roomRepository;
    /**
     * @var ReservationRepository
     */
    private $reservationRepository;

    /**
     * Delete constructor.
     *
     * @param RoomRepository        $roomRepository
     * @param ReservationRepository $reservationRepository
     */
    public function __construct(RoomRepository $roomRepository, ReservationRepository $reservationRepository)
    {
        $this->roomRepository = $roomRepository;
        $this->reservationRepository = $reservationRepository;
    }

    /**
     * @param Room $room
     *
     * @return bool
     *
     * @throws \Exception
     */
    public function execute(Room $room): bool
    {
        $reservations = $this->reservationRepository->findAllByRoom($room);

        if (0 !== $reservations->count()) {
            return false;
        }

        $this->roomRepository->delete($room);

        return true;
    }
}
