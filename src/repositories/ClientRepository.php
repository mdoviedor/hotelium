<?php
/**
 * Created by PhpStorm.
 * User: marlon
 * Date: 9/02/18
 * Time: 07:34 PM.
 */

namespace repositories;

use models\Client;

class ClientRepository
{
    public function paginate()
    {
        return Client::
        paginate(15);
    }

    /**
     * @param Client $client
     */
    public function save(Client $client): void
    {
        $client->save();
    }

    /**
     * @param string    $name
     * @param string    $lastname
     * @param string    $identificationNumber
     * @param string    $identificationType
     * @param int|null  $phone
     * @param int|null  $cellphone
     * @param string    $address
     * @param \DateTime $birthDate
     *
     * @return Client
     */
    public function create(string $name, string $lastname, string $identificationNumber, string $identificationType, ? int $phone, ? int $cellphone, string $address, \DateTime $birthDate): Client
    {
        $client = new Client();

        $client->setNameAttribute($name)
            ->setLastnameAttribute($lastname)
            ->setIdentificationTypeAttribute($identificationType)
            ->setIdentificationNumberAttribute($identificationNumber)
            ->setPhoneAttribute($phone)
            ->setCellphoneAttribute($cellphone)
            ->setAddressAttribute($address)
            ->setBirthDateAttribute($birthDate);

        $this->save($client);

        return $client;
    }

    /**
     * @param int $id
     *
     * @return Client
     */
    public function findById(int $id): Client
    {
        return Client::find($id) ?? abort(404);
    }

    public function findAll()
    {
        return Client::all();
    }
}
