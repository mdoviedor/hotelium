<?php
/**
 * Created by PhpStorm.
 * User: marlon
 * Date: 8/02/18
 * Time: 10:59 PM.
 */

namespace repositories;

use models\Room;

class RoomRepository
{
    /**
     * @return static
     */
    public function paginate()
    {
        return Room::
        paginate(15);
    }

    /**
     * @param Room $room
     */
    public function save(Room $room): void
    {
        $room->save();
    }

    /**
     * @param Room $room
     *
     * @throws \Exception
     */
    public function delete(Room $room): void
    {
        $room->delete();
    }

    /**
     * @param int         $number
     * @param int         $floorNumber
     * @param string      $type
     * @param bool        $airConditioner
     * @param null|string $comments
     *
     * @return Room
     */
    public function create(int $number, int $floorNumber, string $type, bool $airConditioner, int $numberBeds, ?  string $comments): Room
    {
        $room = new Room();
        $room->setNumberAttribute($number)
            ->setFloorNumberAttribute($floorNumber)
            ->setTypeAttribute($type)
            ->setAirConditionerAttribute($airConditioner)
            ->setCommentsAttribute($comments)
            ->setStatusAttribute(Room::STATUS_FREE)
            ->setNumberBedsAttribute($numberBeds);

        $this->save($room);

        return $room;
    }

    /**
     * @param Room $room
     *
     * @return Room
     */
    public function booked(Room $room): Room
    {
        $room->setStatusAttribute(Room::STATUS_BOOKED);
        $this->save($room);

        return $room;
    }

    /**
     * @param Room $room
     *
     * @return Room
     */
    public function liberate(Room $room): Room
    {
        $room->setStatusAttribute(Room::STATUS_FREE);
        $this->save($room);

        return $room;
    }

    /**
     * @param int $id
     *
     * @return Room
     */
    public function findById(int $id): Room
    {
        return Room::find($id) ?? abort(404);
    }

//    /**
//     * @param string $name
//     * @return mixed
//     */
//    public function paginator(string $name)
//    {
//        $categories = $this->category
//            ->where('name', 'LIKE', "%$name%")
//            ->paginate();
//
//        return $categories;
//    }
}
