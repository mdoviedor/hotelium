<?php
/**
 * Created by PhpStorm.
 * User: marlon
 * Date: 9/02/18
 * Time: 08:58 AM.
 */

namespace repositories;

use Carbon\Carbon;
use models\Client;
use models\Reservation;
use models\Room;

class ReservationRepository
{
    /**
     * @param Reservation $reservation
     */
    public function save(Reservation $reservation)
    {
        $reservation->save();
    }

    /**
     * @param Room $room
     *
     * @return mixed
     */
    public function findAllByRoom(Room $room)
    {
        $reservations = Reservation::where('room_id', $room->id)
            ->orderBy('created_at', 'desc')
            ->get();

        return $reservations;
    }

    /**
     * @param Client $client
     * @param Room   $room
     *
     * @return Reservation
     */
    public function create(Client $client, Room $room): Reservation
    {
        $reservation = new Reservation();

        $now = Carbon::now('America/Bogota');

        $reservation->setDateAttribute($now);
        $reservation->client()->associate($client);
        $reservation->room()->associate($room);

        $this->save($reservation);

        return $reservation;
    }
}
