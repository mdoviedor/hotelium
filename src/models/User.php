<?php
/**
 * Created by PhpStorm.
 * UserSeeder: marlon
 * Date: 7/02/18
 * Time: 09:10 PM.
 */

namespace models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function transaction()
    {
        return $this->hasMany(Transaction::class);
    }
}
