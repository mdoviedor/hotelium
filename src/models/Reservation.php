<?php

namespace models;

use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    protected $fillable = [
        'date', 'room_id', 'client_id',
    ];

    /**
     * @param \DateTime $date
     *
     * @return Reservation
     */
    public function setDateAttribute(\DateTime $date): self
    {
        $this->attributes['date'] = $date;

        return $this;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function room()
    {
        return $this->belongsTo(Client::class);
    }
}
