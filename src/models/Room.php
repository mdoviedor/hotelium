<?php

namespace models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Room.
 */
class Room extends Model
{
    protected $fillable = [
        'number', 'floor_number', 'type', 'number_beds', 'air_conditioner', 'status', 'comments',
    ];

    const STATUS_FREE = 'Libre';

    const STATUS_BOOKED = 'Reservada';

    const STATUS = [
        self::STATUS_BOOKED => self::STATUS_BOOKED,
        self::STATUS_FREE => self::STATUS_FREE,
    ];

    const TYPE_SENCILLA = 'Sencilla';
    const TYPE_DOBLE = 'Doble';
    const TYPE_SUITE = 'Suite';

    const TYPES = [
        self::TYPE_DOBLE => self::TYPE_DOBLE,
        self::TYPE_SENCILLA => self::TYPE_SENCILLA,
        self::TYPE_SUITE => self::TYPE_SUITE,
    ];

    /**
     * @param int $number
     *
     * @return Room
     */
    public function setNumberAttribute(int $number): self
    {
        $this->attributes['number'] = $number;

        return $this;
    }

    /**
     * @param int $floor_number
     *
     * @return Room
     *
     * @internal param int $number
     */
    public function setFloorNumberAttribute(int $floor_number): self
    {
        $this->attributes['floor_number'] = $floor_number;

        return $this;
    }

    /**
     * @param string $type
     *
     * @return Room
     */
    public function setTypeAttribute(string $type): self
    {
        $this->attributes['type'] = $type;

        return $this;
    }

    /**
     * @param int $number_beds
     *
     * @return Room
     */
    public function setNumberBedsAttribute(int $number_beds): self
    {
        $this->attributes['number_beds'] = $number_beds;

        return $this;
    }

    /**
     * @param bool $air_conditioner
     *
     * @return Room
     */
    public function setAirConditionerAttribute(bool $air_conditioner): self
    {
        $this->attributes['air_conditioner'] = $air_conditioner;

        return $this;
    }

    /**
     * @param string $comments
     *
     * @return Room
     */
    public function setCommentsAttribute(string $comments): self
    {
        $this->attributes['comments'] = strtoupper($comments);

        return $this;
    }

    /**
     * @param string $status
     *
     * @return Room
     */
    public function setStatusAttribute(string $status): self
    {
        $this->attributes['status'] = $status;

        return $this;
    }
}
