<?php

namespace models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Client.
 */
class Client extends Model
{
    protected $fillable = [
        'name', 'lastname', 'identification_type', 'identification_number', 'phone', 'cellphone', 'address', 'birth_date',
    ];

    const IDENTIFICATION_CC = 'CC';
    const IDENTIFICATION_CE = 'CE';
    const IDENTIFICATION_TI = 'TI';
    const IDENTIFICATION_PASAPORTE = 'Pasaporte';

    const IDENTIFICATION_TYPES = [
        self::IDENTIFICATION_CC => self::IDENTIFICATION_CC,
        self::IDENTIFICATION_CE => self::IDENTIFICATION_CE,
        self::IDENTIFICATION_TI => self::IDENTIFICATION_TI,
        self::IDENTIFICATION_PASAPORTE => self::IDENTIFICATION_PASAPORTE,
    ];

    /**
     * @param string $name
     *
     * @return Client
     */
    public function setNameAttribute(string $name): self
    {
        $this->attributes['name'] = strtoupper($name);

        return $this;
    }

    /**
     * @param string $lastname
     *
     * @return Client
     */
    public function setLastnameAttribute(string $lastname): self
    {
        $this->attributes['lastname'] = strtoupper($lastname);

        return $this;
    }

    /**
     * @param string $identification_type
     *
     * @return Client
     */
    public function setIdentificationTypeAttribute(string $identification_type): self
    {
        $this->attributes['identification_type'] = $identification_type;

        return $this;
    }

    /**
     * @param string $identification_number
     *
     * @return Client
     */
    public function setIdentificationNumberAttribute(string $identification_number): self
    {
        $this->attributes['identification_number'] = $identification_number;

        return $this;
    }

    /**
     * @param int|null $phone
     *
     * @return Client
     */
    public function setPhoneAttribute(? int $phone): self
    {
        $this->attributes['phone'] = $phone;

        return $this;
    }

    /**
     * @param null|int $cellphone
     *
     * @return Client
     */
    public function setCellphoneAttribute(? int $cellphone): self
    {
        $this->attributes['cellphone'] = $cellphone;

        return $this;
    }

    /**
     * @param string $address
     *
     * @return Client
     */
    public function setAddressAttribute(string $address): self
    {
        $this->attributes['address'] = strtoupper($address);

        return $this;
    }

    /**
     * @param \DateTime $birth_date
     *
     * @return Client
     */
    public function setBirthDateAttribute(\DateTime $birth_date): self
    {
        $this->attributes['birth_date'] = $birth_date;

        return $this;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function reservations()
    {
        return $this->hasMany(Reservation::class);
    }
}
