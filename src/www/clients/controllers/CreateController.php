<?php
/**
 * Created by PhpStorm.
 * User: marlon
 * Date: 8/02/18
 * Time: 08:56 PM.
 */

namespace www\clients\controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use repositories\ClientRepository;

class CreateController
{
    /**
     * @var ClientRepository
     */
    private $clientRepository;

    /**
     * CreateController constructor.
     *
     * @param ClientRepository $clientRepository
     */
    public function __construct(ClientRepository $clientRepository)
    {
        $this->clientRepository = $clientRepository;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function createAction()
    {
        return view('clients::create');
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function storeAction(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:50',
            'lastname' => 'required|string|max:45',
            'identification_type' => 'required|string|max:20',
            'identification_number' => 'required|string|max:20|unique:clients',
            'phone' => 'nullable|integer',
            'cellphone' => 'nullable|integer',
            'address' => 'required|string|max:100',
            'birth_date' => 'required|date|date_format:d-m-Y',
        ]);
        $name = $request->get('name');
        $lastname = $request->get('lastname');
        $identificationType = $request->get('identification_type');
        $identificationNumber = $request->get('identification_number');
        $phone = $request->get('phone');
        $cellphone = $request->get('cellphone');
        $address = $request->get('address');
        $birthDate = Carbon::createFromFormat('d-m-Y', $request->get('birth_date'), 'America/Bogota')->startOfDay();

        $client = $this->clientRepository->create($name, $lastname, $identificationNumber, $identificationType, $phone, $cellphone, $address, $birthDate);

        return redirect()->route('clients-show', [
            'id' => $client->id,
        ]);
    }
}
