<?php
/**
 * Created by PhpStorm.
 * User: marlon
 * Date: 9/02/18
 * Time: 08:37 PM.
 */

namespace www\clients\controllers;

use repositories\ClientRepository;

class ShowController
{
    /**
     * @var ClientRepository
     */
    private $clientRepository;

    /**
     * ShowController constructor.
     *
     * @param ClientRepository $clientRepository
     */
    public function __construct(ClientRepository $clientRepository)
    {
        $this->clientRepository = $clientRepository;
    }

    /**
     * @param string $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function __invoke(string $id)
    {
        $client = $this->clientRepository->findById((int) $id);

        return view('clients::show', [
            'client' => $client,
        ]);
    }
}
