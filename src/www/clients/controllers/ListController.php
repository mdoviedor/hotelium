<?php
/**
 * Created by PhpStorm.
 * User: marlon
 * Date: 8/02/18
 * Time: 08:58 PM.
 */

namespace www\clients\controllers;

use repositories\ClientRepository;

class ListController
{
    /**
     * @var ClientRepository
     */
    private $clientRepository;

    /**
     * ListController constructor.
     *
     * @param ClientRepository $clientRepository
     */
    public function __construct(ClientRepository $clientRepository)
    {
        $this->clientRepository = $clientRepository;
    }

    public function __invoke()
    {
        $clients = $this->clientRepository->paginate();

        return view('clients::list', [
            'clients' => $clients,
        ]);
    }
}
