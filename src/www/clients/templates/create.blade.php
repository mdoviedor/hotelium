@extends('templates::app')

@section('title', 'Clientes')
@section('subtitle', 'Crear')

@section('submenu')
    @include('templates::partials.submenu')
@endsection

@section('content')

    <form class="form-horizontal" method="POST" role="form">
    {{ csrf_field() }}

    @include('templates::forms.horizontal.string', [
        'id' => 'name',
        'name' => 'Nombres: *',
        'placeholder' => 'Nombres'
    ])

    @include('templates::forms.horizontal.string', [
       'id' => 'lastname',
       'name' => 'Apellido: *',
       'placeholder' => 'Número Piso'
    ])

    @include('templates::forms.horizontal.radio', [
        'id' => 'identification_type',
        'name' => 'Tipo de Identificación: *',
        'placeholder' => 'Tipo de Identificación',
        'values' => \models\Client::IDENTIFICATION_TYPES
    ])

    @include('templates::forms.horizontal.string', [
        'id' => 'identification_number',
        'name' => 'Número de Identificación: *',
        'placeholder' => 'Número de Identificación'
    ])

    @include('templates::forms.horizontal.number', [
        'id' => 'phone',
        'name' => 'Télefono:',
        'placeholder' => 'Télefono'
    ])

    @include('templates::forms.horizontal.number', [
        'id' => 'cellphone',
        'name' => 'Móvil:',
        'placeholder' => 'Móvil'
    ])

    @include('templates::forms.horizontal.area', [
        'id' => 'address',
        'name' => 'Dirección: *',
        'placeholder' => 'Dirección'
    ])

    @include('templates::forms.horizontal.string', [
        'id' => 'birth_date',
        'name' => 'Fecha de Nacimiento: *',
        'placeholder' => 'DD-MM-YYYY'
    ])


    @include('templates::forms.horizontal.button_primary', [
        'name' => 'Crear',
    ])


@endsection

@section('script')


            <script type="text/javascript">

                function format(event) {
                    const input = $(event.target);
                    const value = input.val();
                    const countText = value.length;

                    if (countText === 2) {
                        input.val(value + '-');
                    } else if (countText === 5) {
                        // var ano = (new Date()).getFullYear();
                        // input.val(value + '-' + ano);
                    } else if (countText > 10) {
                        input.val(value.substring(0, 10));
                    }
                }

                $(document).ready(() => {
                    $('#birth_date').keyup(format);
                });

            </script>

@endsection