@extends('templates::app')

@section('title', 'Clientes')
@section('subtitle', 'Ver')

@section('submenu')
    @include('templates::partials.submenu')
@endsection

@section('content')

    @include('clients::partials.button_new_client')

    <dl class="dl-horizontal">
        <dt>Nombres</dt>
        <dd>{{ $client->name }}</dd>
        <dt>Apellidos</dt>
        <dd>{{ $client->lastname }}</dd>
        <dt>Tipo ID</dt>
        <dd>{{ $client->identification_type }}</dd>
        <dt>ID</dt>
        <dd>{{ $client->identification_number }}</dd>
        <dt>Télefono</dt>
        <dd>{{ $client->phone ?: "-" }}</dd>
        <dt>Móvil</dt>
        <dd>{{ $client->cellphone ?: "-" }}</dd>
        <dt>Dirección</dt>
        <dd>{{ $client->address ?: "-" }}</dd>
        <dt>Fecha de Nacimiento</dt>
        <dd>{{ $client->birth_date }}</dd>
    </dl>

@endsection