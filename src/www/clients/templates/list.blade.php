@extends('templates::app')

@section('title', 'Clientes')
@section('subtitle', 'Listar')

@section('submenu')
    @include('templates::partials.submenu')
@endsection

@section('content')

    @include('clients::partials.button_new_client')

    <div class="table-responsive">
        <table class="table">
            <thead>
            <tr>
                <th>Nombres</th>
                <th>Apellidos</th>
                <th>Tipo ID</th>
                <th>ID</th>
                <th>Télefono</th>
                <th>Móvil</th>
                <th>Acciones</th>
            </tr>
            </thead>
            <tbody>
            @foreach($clients as $client)
                <tr>
                    <td>{{ $client->name }}</td>
                    <td>{{ $client->lastname }}</td>
                    <td>{{ $client->identification_type }}</td>
                    <td>{{ $client->identification_number }}</td>
                    <td>{{ $client->phone ?: "-" }}</td>
                    <td>{{ $client->cellphone ?: "-" }}</td>
                    <td>
                        <a href="{{ route('clients-show', ['id' => $client->id]) }}" class="btn btn-xs btn-primary">
                            <span class="glyphicon glyphicon-eye-open"></span> ver</a>

                        {{--<a href="{{ route('clients-delete', ['id' => $client->id]) }}" class="btn btn-xs btn-primary">--}}
                            {{--<span class="glyphicon glyphicon-eye-open"></span> ver</a>--}}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    <div class="text-center">
        {{ $clients->render() }}
    </div>
@endsection