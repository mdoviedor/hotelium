<?php
/*
 * Created by PhpStorm.
 * User: marlon
 * Date: 8/02/18
 * Time: 08:56 PM
 */

/**
 * List.
 */
Route::get('/list', [
    'as' => 'clients-list',
    'uses' => \www\clients\controllers\ListController::class,
]);

/*
 * Create
 */
Route::get('/create', [
    'as' => 'clients-create',
    'uses' => \www\clients\controllers\CreateController::class.'@createAction',
]);

Route::post('/create', [
    'as' => 'clients-create-store',
    'uses' => \www\clients\controllers\CreateController::class.'@storeAction',
]);
//
///**
// * Update
// */
//Route::get('/update/{id}', [
//    'as' => 'clients-update',
//    'uses' => \www\clients\controllers\UpdateController::class . "@updateAction"
//]);
//
//Route::post('/update/{id}', [
//    'as' => 'clients-update-store',
//    'uses' => \www\clients\controllers\UpdateController::class . "@storeAction"
//]);
//

/*
 * Show
 */
Route::get('/show/{id}', [
    'as' => 'clients-show',
    'uses' => \www\clients\controllers\ShowController::class,
]);
