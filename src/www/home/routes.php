<?php

Route::get('/', function () {
    return view('home::welcome');
});

Route::get('/home', [
    'as' => 'home',
    'uses' => \www\home\controllers\HomeController::class.'@indexAction',
]);
