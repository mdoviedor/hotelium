<?php

namespace www\home\controllers;

use www\lib\Controller;

/**
 * Class HomeController.
 */
class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexAction()
    {
        return view('home::home');
    }
}
