<?php

namespace www\auth\controllers;

use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use www\lib\Controller;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controllers is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controllers instance.
     */
    public function __construct()
    {
        $this->middleware('guest');
    }
}
