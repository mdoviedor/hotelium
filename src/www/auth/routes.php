<?php

// Password Reset Routes...
Route::get('password/reset', [
    'uses' => \www\auth\controllers\ForgotPassrdController::class.'@showLinkRequestForm',
    'as' => 'password.request',
]);

Route::post('password/email', [
    'uses' => \www\auth\controllers\ForgotPassrdController::class.'@sendResetLinkEmail',
    'as' => 'password.email',
]);

Route::get('password/reset/{token}', [
    'uses' => \www\auth\controllers\ResetPasswordController::class.'@showResetForm',
    'as' => 'password.reset',
]);

Route::post('password/reset', [
    'uses' => \www\auth\controllers\ResetPasswordController::class.'@reset',
    'as' => 'password.reset',
]);

// Authentication Routes...
Route::get('login', [
    'uses' => \www\auth\controllers\LoginController::class.'@showLoginForm',
    'as' => 'login',
]);

Route::post('login', [
    'uses' => \www\auth\controllers\LoginController::class.'@login',
]);

Route::post('logout', [
    'as' => 'logout',
    'uses' => \www\auth\controllers\LoginController::class.'@logout',
]);

// Registration Routes...
Route::get('register', [
    'uses' => \www\auth\controllers\RegisterController::class.'@showRegistrationForm',
    'as' => 'register',
]);

Route::post('register', [
    'uses' => \www\auth\controllers\RegisterController::class.'@register',
]);
