@extends('templates::app')

@section('title', 'Habitaciones')
@section('subtitle', 'Ver')

@section('submenu')
    @include('templates::partials.submenu')
@endsection

@section('content')

    @include('rooms::partials.button_new_room')
    @include('rooms::partials.info', [ 'room' => $room])


@endsection