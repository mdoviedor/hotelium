@extends('templates::app')

@section('title', 'Habitaciones')
@section('subtitle', 'Reservar')

@section('submenu')
    @include('templates::partials.submenu')
@endsection

@section('content')

    @include('rooms::partials.info', [ 'room' => $room])

    <form class="form-horizontal" method="POST" role="form">
    {{ csrf_field() }}

    @include('templates::forms.horizontal.select', [
        'id' => 'client',
        'name' => 'Cliente: *',
        'placeholder' => 'Cliente',
        'values' => $clients
    ])

    @include('templates::forms.horizontal.button_primary', [
        'name' => 'Realizar Reservación',
    ])

@endsection