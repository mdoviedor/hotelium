@extends('templates::app')

@section('title', 'Habitaciones')
@section('subtitle', 'Listar')

@section('submenu')
    @include('templates::partials.submenu')
@endsection

@section('content')

    @include('rooms::partials.button_new_room')

    <div class="table-responsive">
        <table class="table">
            <thead>
            <tr>
                <th>Número</th>
                <th>Piso</th>
                <th>Tipo</th>
                <th># Camas</th>
                <th>¿Aire Acondicionado?</th>
                <th>Estado</th>
                <th>Acciones</th>
            </tr>
            </thead>
            <tbody>
            @foreach($rooms as $room)
                <tr>
                    <td>{{ $room->number }}</td>
                    <td>{{ $room->floor_number }}</td>
                    <td>{{ $room->type }}</td>
                    <td>{{ $room->number_beds }}</td>
                    <td>{{ $room->air_conditioner ? 'Si' : 'No' }}</td>
                    <td>{{ $room->status  }}</td>
                    <td>
                        <a href="{{ route('rooms-show', ['id' => $room->id]) }}" class="btn btn-xs btn-primary">
                            <span class="glyphicon glyphicon-eye-open"></span> ver</a>

                        @if($room->status == \models\Room::STATUS_FREE )
                            <a href="{{ route('rooms-reservations-make', ['id' => $room->id]) }}"
                               class="btn btn-xs btn-default">
                                <span class="glyphicon glyphicon-check"></span> reservar</a>
                        @else
                            <a id="button_liberate"
                               href="{{ route('rooms-reservations-liberate', ['id' => $room->id]) }}"
                               class="btn btn-xs btn-default">
                                <span class="glyphicon glyphicon-retweet"></span> liberar</a>
                        @endif

                        <a href="{{ route('rooms-delete', ['id' => $room->id]) }}" class="btn btn-xs btn-danger">
                            <span class="glyphicon glyphicon-trash"></span> eliminar</a>

                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    <div class="text-center">
        {{ $rooms->render() }}
    </div>
@endsection

@section('script')

    <script>
        $(document).ready(function () {
            $("#button_liberate").click(function (event) {
                event.preventDefault();

                swal({
                    title: "Usted esta seguro?",
                    text: "Se liberara la habitación ",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                }).then((willDelete) => {
                    if (willDelete) {
                        window.location = $(event.target).attr('href');
                    }

                });

            });

        });
    </script>

@endsection

