<dl class="dl-horizontal">
    <dt>Número</dt>
    <dd>{{ $room->number }}</dd>
    <dt>Piso</dt>
    <dd>{{ $room->floor_number }}</dd>
    <dt># Camas</dt>
    <dd>{{ $room->number_beds }}</dd>
    <dt>Estado</dt>
    <dd>{{ $room->status }}</dd>
    <dt>¿Aire Acondicionado?</dt>
    <dd>{{ $room->air_conditioner ? 'Si' : 'No' }}</dd>
    <dt>Observación</dt>
    <dd>{{ $room->comments }}</dd>
</dl>
