<p class="text-right">
    <a href="{{ route("rooms-create") }}" class="btn btn-primary btn-lg">
        <span class="glyphicon glyphicon-plus"></span> Crear Habitación
    </a>
</p>