@extends('templates::app')

@section('title', 'Habitaciones')
@section('subtitle', 'Crear')

@section('submenu')
    @include('templates::partials.submenu')
@endsection

@section('content')

    <form class="form-horizontal" method="POST" role="form">
    {{ csrf_field() }}

    @include('templates::forms.horizontal.number', [
        'id' => 'number',
        'name' => 'Número: *',
        'placeholder' => 'Número'
    ])

    @include('templates::forms.horizontal.number', [
       'id' => 'floor_number',
       'name' => 'Número Piso: *',
       'placeholder' => 'Número Piso'
    ])

    @include('templates::forms.horizontal.number', [
       'id' => 'number_beds',
       'name' => 'Número de Camas: *',
       'placeholder' => 'Número de Camas'
    ])

    @include('templates::forms.horizontal.radio', [
        'id' => 'type',
        'name' => 'Tipo: *',
        'placeholder' => 'Tipo',
        'values' => \models\Room::TYPES
    ])

    @include('templates::forms.horizontal.radio', [
        'id' => 'air_conditioner',
        'name' => '¿Aire Acondicionado?: *',
        'placeholder' => 'Tipo',
        'values' => [
            true => 'Si',
            false => 'No'
        ]
    ])

    @include('templates::forms.horizontal.area', [
        'id' => 'comments',
        'name' => 'Observación: *',
        'placeholder' => 'Observación'
    ])

    @include('templates::forms.horizontal.button_primary', [
        'name' => 'Crear',
    ])


@endsection