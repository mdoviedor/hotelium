<?php
/**
 * Created by PhpStorm.
 * User: marlon
 * Date: 8/02/18
 * Time: 08:56 PM.
 */

/**
 * List.
 */
Route::get('/list', [
    'as' => 'rooms-list',
    'uses' => \www\rooms\controllers\ListController::class,
]);

/*
 * Create
 */
Route::get('/create', [
    'as' => 'rooms-create',
    'uses' => \www\rooms\controllers\CreateController::class.'@createAction',
]);

Route::post('/create', [
    'as' => 'rooms-create-store',
    'uses' => \www\rooms\controllers\CreateController::class.'@storeAction',
]);
//
///**
// * Update
// */
//Route::get('/update/{id}', [
//    'as' => 'rooms-update',
//    'uses' => \www\rooms\controllers\UpdateController::class . "@updateAction"
//]);
//
//Route::post('/update/{id}', [
//    'as' => 'rooms-update-store',
//    'uses' => \www\rooms\controllers\UpdateController::class . "@storeAction"
//]);
//
/*
 * Show
 */
Route::get('/show/{id}', [
    'as' => 'rooms-show',
    'uses' => \www\rooms\controllers\ShowController::class,
]);

/*
 * Delete
 */
Route::get('/delete/{id}', [
    'as' => 'rooms-delete',
    'uses' => \www\rooms\controllers\DeleteController::class,
]);

//#### RESERVATIONS

/*
 * Make
 */

Route::get('/reservations/{id}/make', [
    'as' => 'rooms-reservations-make',
    'uses' => \www\rooms\controllers\reservations\MakeController::class.'@makeAction',
]);

Route::post('/reservations/{id}/make', [
    'as' => 'rooms-reservations-make-store',
    'uses' => \www\rooms\controllers\reservations\MakeController::class.'@storeAction',
]);

Route::get('/reservations/{id}/liberate', [
    'as' => 'rooms-reservations-liberate',
    'uses' => \www\rooms\controllers\reservations\LiberateController::class,
]);
