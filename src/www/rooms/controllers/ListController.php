<?php
/**
 * Created by PhpStorm.
 * User: marlon
 * Date: 8/02/18
 * Time: 09:02 PM.
 */

namespace www\rooms\controllers;

use repositories\RoomRepository;

class ListController
{
    /**
     * @var RoomRepository
     */
    private $roomRepository;

    /**
     * ListController constructor.
     *
     * @param RoomRepository $roomRepository
     */
    public function __construct(RoomRepository $roomRepository)
    {
        $this->roomRepository = $roomRepository;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function __invoke()
    {
        $rooms = $this->roomRepository->paginate();

        return view('rooms::list', [
            'rooms' => $rooms,
        ]);
    }
}
