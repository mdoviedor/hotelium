<?php
/**
 * Created by PhpStorm.
 * User: marlon
 * Date: 8/02/18
 * Time: 09:01 PM.
 */

namespace www\rooms\controllers;

use Illuminate\Http\Request;
use repositories\RoomRepository;

class CreateController
{
    /**
     * @var RoomRepository
     */
    private $roomRepository;

    /**
     * CreateController constructor.
     *
     * @param RoomRepository $roomRepository
     */
    public function __construct(RoomRepository $roomRepository)
    {
        $this->roomRepository = $roomRepository;
    }

    public function createAction()
    {
        return view('rooms::create');
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function storeAction(Request $request)
    {
        $request->validate([
            'number' => 'required|integer|unique:rooms',
            'floor_number' => 'required|integer',
            'number_beds' => 'required|integer',
            'type' => 'required|string|max:45',
            'air_conditioner' => 'required|boolean',
            'comments' => 'nullable|string|max:255',
        ]);

        $number = $request->get('number');
        $floorNumber = $request->get('floor_number');
        $type = $request->get('type');
        $airConditioner = $request->get('air_conditioner');
        $comments = $request->get('comments');
        $numberBeds = $request->get('number_beds');

        $room = $this->roomRepository->create($number, $floorNumber, $type, $airConditioner, $numberBeds, $comments);

        return redirect()->route('rooms-show', [
            'id' => $room->id,
        ]);
    }
}
