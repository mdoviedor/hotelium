<?php
/**
 * Created by PhpStorm.
 * User: marlon
 * Date: 9/02/18
 * Time: 10:25 PM.
 */

namespace www\rooms\controllers\reservations;

use repositories\RoomRepository;
use www\lib\FlashMessage;

/**
 * Class LiberateController.
 */
class LiberateController
{
    /**
     * @var RoomRepository
     */
    private $roomRepository;
    /**
     * @var FlashMessage
     */
    private $flashMessage;

    /**
     * LiberateController constructor.
     *
     * @param RoomRepository $roomRepository
     * @param FlashMessage   $flashMessage
     */
    public function __construct(RoomRepository $roomRepository, FlashMessage $flashMessage)
    {
        $this->roomRepository = $roomRepository;
        $this->flashMessage = $flashMessage;
    }

    /**
     * @param string $id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function __invoke(string $id)
    {
        $room = $this->roomRepository->findById((int) $id);
        $this->roomRepository->liberate($room);
        $this->flashMessage->execute("Habitación {$room->number} liberada");

        return redirect()->route('rooms-list');
    }
}
