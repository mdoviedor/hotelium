<?php
/**
 * Created by PhpStorm.
 * User: marlon
 * Date: 9/02/18
 * Time: 09:43 PM.
 */

namespace www\rooms\controllers\reservations;

use Illuminate\Http\Request;
use repositories\ClientRepository;
use repositories\ReservationRepository;
use repositories\RoomRepository;
use www\lib\FlashMessage;

/**
 * Class MakeController.
 */
class MakeController
{
    /**
     * @var RoomRepository
     */
    private $roomRepository;
    /**
     * @var ClientRepository
     */
    private $clientRepository;
    /**
     * @var ReservationRepository
     */
    private $reservationRepository;
    /**
     * @var FlashMessage
     */
    private $flashMessage;

    /**
     * MakeController constructor.
     *
     * @param RoomRepository        $roomRepository
     * @param ClientRepository      $clientRepository
     * @param ReservationRepository $reservationRepository
     * @param FlashMessage          $flashMessage
     */
    public function __construct(RoomRepository $roomRepository, ClientRepository $clientRepository, ReservationRepository $reservationRepository, FlashMessage $flashMessage)
    {
        $this->roomRepository = $roomRepository;
        $this->clientRepository = $clientRepository;
        $this->reservationRepository = $reservationRepository;
        $this->flashMessage = $flashMessage;
    }

    /**
     * @return array
     */
    private function getClients(): array
    {
        $clients = $this->clientRepository->findAll();

        $data = [];
        foreach ($clients as $client) {
            $data[$client->id] = "$client->identification_number - $client->name $client->lastname";
        }

        return $data;
    }

    /**
     * @param string $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function makeAction(string $id)
    {
        $room = $this->roomRepository->findById((int) $id);

        return view('rooms::reservations.make', [
            'room' => $room,
            'clients' => $this->getClients(),
        ]);
    }

    /**
     * @param Request $request
     * @param string  $id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function storeAction(Request $request, string $id)
    {
        $request->validate([
            'client' => 'required|string',
        ]);

        $clientId = $request->get('client');

        $room = $this->roomRepository->findById((int) $id);
        $client = $this->clientRepository->findById((int) $clientId);

        $this->reservationRepository->create($client, $room);
        $this->roomRepository->booked($room);

        $this->flashMessage->execute("Habitación {$room->number} reservada por {$client->name}");

        return redirect()->route('rooms-list');
    }
}
