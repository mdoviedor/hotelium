<?php
/**
 * Created by PhpStorm.
 * User: marlon
 * Date: 8/02/18
 * Time: 09:10 PM.
 */

namespace www\rooms\controllers;

use repositories\RoomRepository;

/**
 * Class ShowController.
 */
class ShowController
{
    /**
     * @var RoomRepository
     */
    private $roomRepository;

    /**
     * ShowController constructor.
     *
     * @param RoomRepository $roomRepository
     */
    public function __construct(RoomRepository $roomRepository)
    {
        $this->roomRepository = $roomRepository;
    }

    /**
     * @param string $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function __invoke(string $id)
    {
        $room = $this->roomRepository->findById((int) $id);

        return view('rooms::show', [
            'room' => $room,
        ]);
    }
}
