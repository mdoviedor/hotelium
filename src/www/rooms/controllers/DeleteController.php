<?php
/**
 * Created by PhpStorm.
 * User: marlon
 * Date: 8/02/18
 * Time: 09:02 PM.
 */

namespace www\rooms\controllers;

use repositories\RoomRepository;
use use_cases\rooms\Delete;
use www\lib\FlashMessage;

/**
 * Class DeleteController.
 */
class DeleteController
{
    /**
     * @var Delete
     */
    private $delete;
    /**
     * @var RoomRepository
     */
    private $roomRepository;
    /**
     * @var FlashMessage
     */
    private $flashMessage;

    /**
     * DeleteController constructor.
     *
     * @param Delete         $delete
     * @param RoomRepository $roomRepository
     * @param FlashMessage   $flashMessage
     */
    public function __construct(Delete $delete, RoomRepository $roomRepository, FlashMessage $flashMessage)
    {
        $this->delete = $delete;
        $this->roomRepository = $roomRepository;
        $this->flashMessage = $flashMessage;
    }

    /**
     * @param string $id
     *
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Exception
     */
    public function __invoke(string $id)
    {
        $room = $this->roomRepository->findById((int) $id);

        $isDelete = $this->delete->execute($room);

        $message = 'Habitación eliminada';

        if (!$isDelete) {
            $message = "No es posible eliminar la habitación {$room->number}, cuenta con reservaciones previas.";
        }

        $this->flashMessage->execute($message);

        return redirect()->route('rooms-list');
    }
}
