<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Hotelium') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
<div id="app">

    <br>
    @include('templates::partials.nav')


    <div class="container">

        <div class="page-header">
            <h1>
                @yield('title')
                <small>@yield('subtitle')</small>
            </h1>
        </div>

        @yield('submenu')
        @yield('content')
    </div>


</div>

<!-- Scripts -->
<script src="{{ asset('js/app.js') }}"></script>


@if( Session::has('flash_message'))
    <script>
        swal("{{ Session::get('flash_message')  }}");
    </script>
@endif

@yield('script')
</body>
</html>
