<div class="form-group{{ $errors->has("$id") ? ' has-error' : '' }}">
    <label for="{{ $id  }}" class="col-sm-2 control-label">{{ $name }}</label>
    <div class="col-sm-10">

        <select class="form-control" name="{{ $id }}" id="{{ $id }}">
            @foreach($values as $key =>  $value)
                <option value="{{ $key }}">{{ $value }}</option>
            @endforeach

        </select>
        @if ($errors->has("$id"))
            <span class="help-block">
                <strong>{{ $errors->first("$id") }}</strong>
            </span>
        @endif
    </div>
</div>