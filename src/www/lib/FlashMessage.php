<?php
/**
 * Created by PhpStorm.
 * User: marlon
 * Date: 9/02/18
 * Time: 08:54 AM.
 */

namespace www\lib;

/**
 * Class FlashMessage.
 */
class FlashMessage
{
    /**
     * @param string $message
     */
    public function execute(string $message)
    {
        \Session::flash('flash_message', $message);
    }
}
