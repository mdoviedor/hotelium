<?php
/**
 * Created by PhpStorm.
 * UserSeeder: marlon
 * Date: 7/02/18
 * Time: 09:42 PM.
 */

namespace www\lib;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
}
